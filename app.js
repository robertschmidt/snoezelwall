var express = require('express');
var path = require('path');
var app = express();
var proc = require('child_process');

var PUBLIC_DIR = path.join(__dirname, 'public');

// Serve the static HTML content
app.use('/', express.static(PUBLIC_DIR));

var server = app.listen(8080, '127.0.0.1', function () {
	var host = server.address().address;
	var port = server.address().port;
	console.log("listening at http://%s:%s %s", host, port, PUBLIC_DIR)
	//proc.exec('taskkill /F /IM chrome.exe');
	proc.exec('start chrome --autoplay-policy=no-user-gesture-required --kiosk http://localhost:' + port);
})